import re
import json
from urllib.parse import quote, urlencode
import scrapy
from itemloaders import ItemLoader
from itemloaders.processors import TakeFirst, MapCompose
from scrapy.http.request import Request

from solution.solution1 import price_from_string
from solution.solution2 import regex_dims
from scrapy.crawler import CrawlerProcess
from price_parser import Price


def price_from_string(price: str) -> float:
    return Price.fromstring(price).amount_float


class ArtItem(scrapy.Item):
    url = scrapy.Field()
    title = scrapy.Field()
    media = scrapy.Field()
    height = scrapy.Field()
    width = scrapy.Field()
    price = scrapy.Field()


class ArtItemLoader(ItemLoader):
    default_item_class = ArtItem
    default_output_processor = TakeFirst()
    price_in = MapCompose(price_from_string)


NEXT_PAGE_QUERY = "query,getFilteredProducts($mainCollectionId:String!,$filters:ProductFilters,$sort:ProductSort,$offset:Int,$limit:Int,$withOptions:Boolean,=,false,$withPriceRange:Boolean,=,false){catalog{category(categoryId:$mainCollectionId){numOfProducts,productsWithMetaData(filters:$filters,limit:$limit,sort:$sort,offset:$offset,onlyVisible:true){totalCount,list{id,options{id,key,title,@include(if:$withOptions),optionType,@include(if:$withOptions),selections,@include(if:$withOptions){id,value,description,key,linkedMediaItems{url,fullUrl,thumbnailFullUrl:fullUrl(width:50,height:50),mediaType,width,height,index,title,videoFiles{url,width,height,format,quality}}}}productItems,@include(if:$withOptions){id,optionsSelections,price,formattedPrice,formattedComparePrice,inventory{status,quantity}isVisible,pricePerUnit,formattedPricePerUnit}customTextFields(limit:1){title}productType,ribbon,price,comparePrice,sku,isInStock,urlPart,formattedComparePrice,formattedPrice,pricePerUnit,formattedPricePerUnit,pricePerUnitData{baseQuantity,baseMeasurementUnit}digitalProductFileItems{fileType}name,media{url,index,width,mediaType,altText,title,height}isManageProductItems,isTrackingInventory,inventory{status,quantity}subscriptionPlans{list{id,visible}}priceRange(withSubscriptionPriceRange:true),@include(if:$withPriceRange){fromPriceFormatted}discount{mode,value}}}}}}"


class BearspaceSpider(scrapy.Spider):
    name = 'bearspace'
    allowed_domains = ['bearspace.co.uk']
    start_urls = ['https://www.bearspace.co.uk/purchase']
    custom_settings = {
        'FEEDS': {
            'bearspace_data.csv': {
                'format': 'json',
            }
        },
        # pagination URLS are above limit
        'URLLENGTH_LIMIT': 0,
        'USER_AGENT': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/97.0.4692.71 Safari/537.36'
    }

    def start_requests(self):
        # run with one artwork, just for debugging, to check some specific urls
        if getattr(self, 'single_url', None):
            yield scrapy.Request(self.single_url, callback=self.parse_artwork)
            return
        for x in super().start_requests():
            yield x

    def parse(self, response):
        # this works
        for x in response.css('section[data-hook=product-list] li a::attr(href)').extract():
            yield response.follow(x, callback=self.parse_artwork)

        # this doesn't work
        yield self.next_page(response)

    def next_page(self, response):
        # TODO this needs to be done properly, need to handle
        # authorization header properly, figure out how it is generated
        set_cookie_header = response.headers.get('set-cookie')
        xsrf = re.search("XSRF-TOKEN=(.+?);", set_cookie_header.decode())
        if not xsrf:
            self.logger.error(
                f"No xsrf on page {response.url}, cannot paginate")
            return

        xsrf = xsrf.group(1)

        headers = {
            'content-type': 'application/json; charset=utf-8',
            'X-XSRF-TOKEN': xsrf,
            # TODO this is hardcoded, figure out how to generate it in spider
            'Authorization': 'E-shV62ssfTpkI2HM90jbXMHrv6d2cwnGVfaa8FaMZ0.eyJpbnN0YW5jZUlkIjoiOWRiZDIzZjktMzE0YS00NzVjLWI4OTAtYTZhNjQ1ZGNiZTdhIiwiYXBwRGVmSWQiOiIxMzgwYjcwMy1jZTgxLWZmMDUtZjExNS0zOTU3MWQ5NGRmY2QiLCJtZXRhU2l0ZUlkIjoiOGQ3ODQxYzctNmFkMC00MjdkLTg5NWMtMzFkYzE0ODhmYWVlIiwic2lnbkRhdGUiOiIyMDIyLTAxLTExVDE3OjI0OjA3LjMzNloiLCJ2ZW5kb3JQcm9kdWN0SWQiOiJQcmVtaXVtMSIsImRlbW9Nb2RlIjpmYWxzZSwib3JpZ2luSW5zdGFuY2VJZCI6ImJjYjk0MjcyLWU2ZWEtNDc1YS05ZThlLWZjYTEwMDI4NzZlYiIsImFpZCI6ImM4YTA3NTZmLTljMTMtNDQxZC1hNTVkLTRlM2IwN2MzODZkYyIsImJpVG9rZW4iOiIxMGM1NjIzZS01YjlhLTA1MjEtMzFjYy05NzdhNTE1NDQ0OTQiLCJzaXRlT3duZXJJZCI6IjIzYTJlZDE4LWMyYTgtNDNlZi04ZmIzLWZmYTBjMjQzNGYyOCJ9'
        }
        page_number = response.meta.get('page_number', 1)
        offset = 20 * page_number
        collection_id = '00000000-000000-000000-000000000001'
        next_page_obj = {"mainCollectionId": collection_id, "offset": offset,
                         "limit": 20, "sort": None, "filters": None, "withOptions": False, "withPriceRange": False}
        query = {
            'q': NEXT_PAGE_QUERY,
            's': 'WixStoresWebClient',
            'o': 'getFilteredProducts',
            'v': quote(json.dumps(next_page_obj))
        }
        api_url = "https://www.bearspace.co.uk/_api/wix-ecommerce-storefront-web/api?"
        next_page_url = api_url + urlencode(query)
        return Request(
            next_page_url, headers=headers, callback=self.parse_api,
            meta={'handle_httpstatus_all': True}
            )

    def parse_artwork(self, response):
        loader = ArtItemLoader(selector=response)
        title_css = 'h1[data-hook=product-title]::text'
        loader.add_css('title', title_css)
        # relying on unsemantic html, can be error prone but
        # don't see other way here
        spans = " ".join(response.css(
            '*[data-hook=description] p ::text').extract())

        # this won't extract 120x120, dimension without unit of measure
        # TODO extract it, update regex above for it
        height, width, depth = regex_dims(spans)
        # media, I assume media is all string until first dimension
        # this is error prone, but hard to find better way
        if height:
            loader.add_value('media', spans, re=f'(.+)\s{int(height)}')
        loader.add_css(
            'price',
            'span[data-hook=formatted-primary-price]::text'
        )
        loader.add_value('height', height)
        loader.add_value('width', width)
        loader.add_value('url', response.url)
        return loader.load_item()

    def parse_api(self, response):
        # TODO
        # handling authorization header properly, now it returns 500 without it
        pass


if __name__ == "__main__":
    import sys
    if len(sys.argv) > 1:
        single_url = sys.argv[1]
    else:
        single_url = None
    process = CrawlerProcess()
    process.crawl(BearspaceSpider, single_url=single_url)
    process.start()
