

from os import path
from scrapy.http.response.html import HtmlResponse
import pytest
from solution.bearspace import BearspaceSpider
from solution.solution1 import parse_html
from solution.solution2 import regex_dims


def data_path(filename):
    return path.join(
        'solution', 'data', filename
    )


def test_parse_html():
    fpath = data_path('webpage.html')
    with open(fpath) as f:
        output = parse_html(f.read())

    assert len(output) == 8
    expected = {
        "artist": "Peter Doig",
        "title": "The Architect's Home in the Ravine",
        "Price realised GBP": 11282500,
        "Price realised USD": 16370908,
        "Estimates GBP": [10000000, 15000000],
        "Estimate USD": [14509999, 21764999],
        "image url": "http://www.christies.com/lotfinderimages/D59730/peter_doig_the_architects_home_in_the_ravine_d5973059g.jpg",
        "Saledate": "2016-02-11T00:00:00"
    }
    for key, value in expected.items():
        assert output[key] == value


@pytest.mark.parametrize('dimensions,width,height,depth', [
    ('19×52cm', 19.0, 52.0, None),
    ('50 x 66,4 cm', 50.0, 66.4, None),
    ('168.9 x 274.3 x 3.8 cm (66 1/2 x 108 x 1 1/2 in.)',  168.9, 274.3, 3.8),
    ('Sheet: 16 1/4 × 12 1/4 in. (41.3 × 31.1 cm) Image: 14 × 9 7/8 in. (35.6 × 25.1 cm)',  35.6, 25.1, None),
    ('5 by 5in', 12.7, 12.7, None)
])
def test_regex_dims(dimensions, width, height, depth):
    result = regex_dims(dimensions) 
    if depth is None and len(result) == 2:
        result.append(None)
    assert result == [width, height, depth]


@pytest.mark.parametrize('filename,expected', [
    ('p10.html', {
        'title': 'P10 by Olly Fathers',
        'media': 'Acrylic paint on sprayed board',
        'price': 240,
        'height': 30,
        'width': 30
    })
])
def test_scraper(filename, expected):
    spider = BearspaceSpider()
    with open(data_path(filename)) as f:
        response = HtmlResponse('http://aa', body=f.read().encode())
    item = spider.parse_artwork(response)
    for key, value in expected.items():
        assert item[key] == value
