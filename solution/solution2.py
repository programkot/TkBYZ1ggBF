import re


def float_conv(txt: str) -> float:
    if not txt:
        return

    return float(txt.replace(",", "."))


def inch_to_cm(num: float, convert: bool) -> float:
    if num is None:
        return
    return num * 2.54 if convert else num



def regex_dims(text: str) -> list:
    # pieces of regex
    # regex for separator, matches '×' or "x" or "by"
    sep = r"(?:" + "|".join([chr(215), "x", "by"]) + ")"

    # regex for digits, matches 12.00, 12,00, 12
    digits = r"([\d\,\.]+)"

    # regex for unit for later unit conversion
    unit = "(cm|in)"

    # regex for space
    space = r"\s"

    # Bonus: Is there a single regex for all 5 examples?
    # Should be possible for all, just need bit more time on one with Image
    # and of course conversion between inches and cm not possible via regex
    
    
    # special case of "Sheet: 12x12 Image: 14 x 14"
    if "Image" in text:
        # Manual hack for string with Image, probably possible to handle with regex too
        # but question if extra complexity and runtime of regex customized for all
        # possible edge cases is worth it? Writing regex for all even most weird string
        # can take really long time. 
        text = text.split("Image")[-1]
        # If we need regex just for this it would be something like 
        # (?:Image.?) + rest of regex for rest of cases

    two_dims = r'{digits}{space}?{sep}{space}?{digits}'.format(
        space=space, digits=digits, sep=sep
    )
    more_dims = r'{space}?{sep}?{space}?{digits}?{space}?{unit}'.format(
        digits=digits, sep=sep, space=space, unit=unit
    )
    # Regex for all examples
    # matches all strings
    dims_with_image = two_dims + more_dims

    result = re.search(dims_with_image, text)

    if not result:
        return [None, None, None]

    result_groups = result.groups()
    unit = result_groups[-1]
    # converts inches to floats, as defined in specs.
    result_float = list(map(float_conv, result_groups[:3]))
    return [inch_to_cm(r, unit == 'in') for r in result_float]
