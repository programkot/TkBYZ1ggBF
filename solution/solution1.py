from parsel import Selector
from price_parser import Price
import dateparser


def price_from_string(price: str) -> float:
    return Price.fromstring(price).amount_float


def ests_parse(est_list: list) -> list:
    return list(map(price_from_string, est_list))


def parse_html(text: str) -> dict:
    sel = Selector(text=text)
    output = {}
    output['artist'] = sel.css('h1.lotName::text').re_first(r'(.+)\s\(')
    output['title'] = sel.css('.itemName i::text').get().strip()
    price_gb = sel.css(
        'span[id$=blPriceRealizedPrimary]::text'
    ).get()
    output["Price realised GBP"] = price_from_string(price_gb)
    price_us = sel.css(
        '*[id$=lblPriceRealizedSecondary]::text'
    ).get()
    
    # there is a mistake in description, price realizeed is 16 mln
    # not 6 mln. that would be well below estimates, owner of painting
    # would not be happy.
    output["Price realised USD"] = price_from_string(price_us)
    ests_gb = sel.css(
        '*[id$=lblPriceEstimatedPrimary]::text'
    ).get()

    output["Estimates GBP"] = ests_parse(ests_gb.split('-'))

    ests_us = sel.css(
        '*[id$=lblPriceEstimatedSecondary]::text'
    ).get()
    output["Estimate USD"] = ests_parse(ests_us.split("-"))
    # Take largest image, best possible quality
    output["image url"] = sel.css(
        '.box-link a::attr(data-carousel-lg)'
    ).get()
    pub_date = sel.css(
        '*[id$=lblSaleDate]::text'
    ).get()
    output["Saledate"] = dateparser.parse(pub_date).isoformat()
    return output
