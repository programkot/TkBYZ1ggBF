# What is it

Solutions to the challenge are located in /solution directory. Each solution is paired with unit test. To run unit tests simply do

```
> pytest solution
```


to run spider

```
python -m solution.bearspace
```

I assumed this is a simple site so wrote it as one spider, but it is somewhat more complex, and should probably be a saparate project.


Code uses Python 3.10