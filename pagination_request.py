import requests

headers = {
    'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/97.0.4692.71 Safari/537.36',
    'Referer': 'https://www.bearspace.co.uk/_partials/wix-thunderbolt/dist/clientWorker.6f821593.bundle.min.js',
    'DNT': '1',
    'X-XSRF-TOKEN': '1641921847|JvHbWFdfj2rW',
    'Authorization': 'E-shV62ssfTpkI2HM90jbXMHrv6d2cwnGVfaa8FaMZ0.eyJpbnN0YW5jZUlkIjoiOWRiZDIzZjktMzE0YS00NzVjLWI4OTAtYTZhNjQ1ZGNiZTdhIiwiYXBwRGVmSWQiOiIxMzgwYjcwMy1jZTgxLWZmMDUtZjExNS0zOTU3MWQ5NGRmY2QiLCJtZXRhU2l0ZUlkIjoiOGQ3ODQxYzctNmFkMC00MjdkLTg5NWMtMzFkYzE0ODhmYWVlIiwic2lnbkRhdGUiOiIyMDIyLTAxLTExVDE3OjI0OjA3LjMzNloiLCJ2ZW5kb3JQcm9kdWN0SWQiOiJQcmVtaXVtMSIsImRlbW9Nb2RlIjpmYWxzZSwib3JpZ2luSW5zdGFuY2VJZCI6ImJjYjk0MjcyLWU2ZWEtNDc1YS05ZThlLWZjYTEwMDI4NzZlYiIsImFpZCI6ImM4YTA3NTZmLTljMTMtNDQxZC1hNTVkLTRlM2IwN2MzODZkYyIsImJpVG9rZW4iOiIxMGM1NjIzZS01YjlhLTA1MjEtMzFjYy05NzdhNTE1NDQ0OTQiLCJzaXRlT3duZXJJZCI6IjIzYTJlZDE4LWMyYTgtNDNlZi04ZmIzLWZmYTBjMjQzNGYyOCJ9',
    'Content-Type': 'application/json; charset=utf-8',
}

params = (
    ('o', 'getFilteredProducts'),
    ('s', 'WixStoresWebClient'),
    ('q', 'query,getFilteredProducts($mainCollectionId:String/u0021,$filters:ProductFilters,$sort:ProductSort,$offset:Int,$limit:Int,$withOptions:Boolean,=,false,$withPriceRange:Boolean,=,false)/{catalog/{category(categoryId:$mainCollectionId)/{numOfProducts,productsWithMetaData(filters:$filters,limit:$limit,sort:$sort,offset:$offset,onlyVisible:true)/{totalCount,list/{id,options/{id,key,title,@include(if:$withOptions),optionType,@include(if:$withOptions),selections,@include(if:$withOptions)/{id,value,description,key,linkedMediaItems/{url,fullUrl,thumbnailFullUrl:fullUrl(width:50,height:50),mediaType,width,height,index,title,videoFiles/{url,width,height,format,quality/}/}/}/}productItems,@include(if:$withOptions)/{id,optionsSelections,price,formattedPrice,formattedComparePrice,inventory/{status,quantity/}isVisible,pricePerUnit,formattedPricePerUnit/}customTextFields(limit:1)/{title/}productType,ribbon,price,comparePrice,sku,isInStock,urlPart,formattedComparePrice,formattedPrice,pricePerUnit,formattedPricePerUnit,pricePerUnitData/{baseQuantity,baseMeasurementUnit/}digitalProductFileItems/{fileType/}name,media/{url,index,width,mediaType,altText,title,height/}isManageProductItems,isTrackingInventory,inventory/{status,quantity/}subscriptionPlans/{list/{id,visible/}/}priceRange(withSubscriptionPriceRange:true),@include(if:$withPriceRange)/{fromPriceFormatted/}discount/{mode,value/}/}/}/}/}/}'),
    ('v', '{"mainCollectionId":"00000000-000000-000000-000000000001","offset":60,"limit":20,"sort":null,"filters":null,"withOptions":false,"withPriceRange":false}'),
)

response = requests.get('https://www.bearspace.co.uk/_api/wix-ecommerce-storefront-web/api', headers=headers, params=params)
response.raise_for_status()
print(response.body)

#NB. Original query string below. It seems impossible to parse and
#reproduce query strings 100% accurately so the one below is given
#in case the reproduced version is not "correct".
# response = requests.get('$https://www.bearspace.co.uk/_api/wix-ecommerce-storefront-web/api?o=getFilteredProducts&s=WixStoresWebClient&q=query,getFilteredProducts($mainCollectionId:String\u0021,$filters:ProductFilters,$sort:ProductSort,$offset:Int,$limit:Int,$withOptions:Boolean,=,false,$withPriceRange:Boolean,=,false)\{catalog\{category(categoryId:$mainCollectionId)\{numOfProducts,productsWithMetaData(filters:$filters,limit:$limit,sort:$sort,offset:$offset,onlyVisible:true)\{totalCount,list\{id,options\{id,key,title,@include(if:$withOptions),optionType,@include(if:$withOptions),selections,@include(if:$withOptions)\{id,value,description,key,linkedMediaItems\{url,fullUrl,thumbnailFullUrl:fullUrl(width:50,height:50),mediaType,width,height,index,title,videoFiles\{url,width,height,format,quality\}\}\}\}productItems,@include(if:$withOptions)\{id,optionsSelections,price,formattedPrice,formattedComparePrice,inventory\{status,quantity\}isVisible,pricePerUnit,formattedPricePerUnit\}customTextFields(limit:1)\{title\}productType,ribbon,price,comparePrice,sku,isInStock,urlPart,formattedComparePrice,formattedPrice,pricePerUnit,formattedPricePerUnit,pricePerUnitData\{baseQuantity,baseMeasurementUnit\}digitalProductFileItems\{fileType\}name,media\{url,index,width,mediaType,altText,title,height\}isManageProductItems,isTrackingInventory,inventory\{status,quantity\}subscriptionPlans\{list\{id,visible\}\}priceRange(withSubscriptionPriceRange:true),@include(if:$withPriceRange)\{fromPriceFormatted\}discount\{mode,value\}\}\}\}\}\}&v=%7B%22mainCollectionId%22%3A%2200000000-000000-000000-000000000001%22%2C%22offset%22%3A60%2C%22limit%22%3A20%2C%22sort%22%3Anull%2C%22filters%22%3Anull%2C%22withOptions%22%3Afalse%2C%22withPriceRange%22%3Afalse%7D', headers=headers)
