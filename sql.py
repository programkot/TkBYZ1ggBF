import pandas as pd
flights = pd.read_csv("candidateEvalData/flights.csv")
airports = pd.read_csv("candidateEvalData/airports.csv")
weather = pd.read_csv("candidateEvalData/weather.csv")
airlines = pd.read_csv("candidateEvalData/airlines.csv")

flights.head(3)


# Add full airline name to the flights dataframe and show the arr_time, origin, dest and the name of the airline.
# with sqlite
# > alter table flights add column full_carrier_name;
#  update flights set full_carrier_name =(select name from airlines where carrier = flights.carrier);
# > select arr_time, origin, dest, full_carrier_name, carrier from flights;

# Filter resulting data.frame to include only flights containing the word JetBlue
# > select * from flights where full_carrier_name like "%Jet%";

# Summarise the total number of flights by origin in ascending.
# answer: select origin, carrier, count(carrier) cc from flights group by origin order by cc

#  Filter resulting data.frame to return only origins with more than 10,000 flights.
# there is just 21 flights? probably error in trial?